local sensorInfo = {
	name = "filterTransportable",
	desc = "Return unitID of units from the list, that are transportable by a transporer, and that are not in a safe area.",
	author = "Jiri Krejci",
	date = "2022-06-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function isTransportable(unitID)
	return not UnitDefs[Spring.GetUnitDefID(unitID)].cantBeTransported
end

function isNotSafe(unitID, safeArea)
	local x,y,z = Spring.GetUnitBasePosition(unitID)
	return (Vec3(x,0,z) - Vec3(safeArea.center.x, 0, safeArea.center.z)):Length() > safeArea.radius
end

return function(unitsList, safeArea)
    transportableUnits = {}
	local myTeam = Spring.GetLocalTeamID()

    for i = 1, #unitsList do
        local unit = unitsList[i]
        if isTransportable(unit) and isNotSafe(unit, safeArea) then
            table.insert(transportableUnits,unit)
        end
    end

    return transportableUnits
end
