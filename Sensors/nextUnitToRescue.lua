local sensorInfo = {
	name = "nextUnitToRescue",
	desc = "Returns the unitID of the next unit to rescue.",
	author = "Jiri Krejci",
	date = "2022-06-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function isTransportable(unitID)
	return not UnitDefs[Spring.GetUnitDefID(unitID)].cantBeTransported
end

return function(unitsList)
	local myTeam = Spring.GetLocalTeamID()
    if bb.idx == nil then
        bb.idx = 0
    end


    if bb.idx == #unitsList then
		return -1
	else
		bb.idx = bb.idx + 1
	end

	return unitsList[bb.idx]
end