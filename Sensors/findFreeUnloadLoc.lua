local sensorInfo = {
	name = "nextUnitToRescue",
	desc = "Returns the unitID of the next unit to rescue.",
	author = "Jiri Krejci",
	date = "2022-06-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function isTransportable(unitID)
	return not UnitDefs[Spring.GetUnitDefID(unitID)].cantBeTransported
end

function FindFreePlace(safeX, safeZ, radius, resTable)
    local x, z
    while true do
        x = safeX + math.random(-radius, radius)
        z = safeZ + math.random(-radius, radius)
        if (Vec3(x, 0, z)-Vec3(safeX,0,safeZ)):Length() < radius and isEmpty(x,z)
           and notReserved(x,z, resTable) then
            break
        end
    end

    return x, z
end

function isEmpty(x,z)
    local radius = 80
    return #Spring.GetUnitsInCylinder(x, z, radius) == 0
end

function notReserved(x, z, resTable)
    for index, value in ipairs(resTable) do
        if (Vec3(x,0,z) - Vec3(value.x,0,value.z)):Length() < 80 then
            return false
        end
    end
    return true
end

return function(unloadLoc, resTable)
    x, z = FindFreePlace(unloadLoc.center.x, unloadLoc.center.z, unloadLoc.radius - 25, resTable)
    table.insert(resTable, {x=x, z=z})
    return {x=x, y=unloadLoc.center.y, z=z}
end