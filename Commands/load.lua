function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Load all units.",
        parameterDefs = {
            {
                name = "transporterID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

-- Loads a single unit into a transporter.
function Run(self, units, parameter)
    local unitID = parameter.unitID
    local transporterID = parameter.transporterID

    -- transporter dead or invalid
    if not Spring.ValidUnitID(transporterID) then
        return SUCCESS
    end

    -- unit to rescue dead or invalid
    if not Spring.ValidUnitID(unitID) then
        return SUCCESS
    end

    -- transporter can transport units
    if not UnitDefs[Spring.GetUnitDefID(transporterID)].isTransport then
        return FAILURE
    end

    if not self.initialized then
        self.initialized = true
        Spring.GiveOrderToUnit(transporterID, CMD.LOAD_UNITS,{unitID},{})
    end

    if #Spring.GetUnitIsTransporting(transporterID) == 1 then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end