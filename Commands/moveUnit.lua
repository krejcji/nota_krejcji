function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Move unit to a given location.",
        parameterDefs = {
            {
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "pos",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

local THRESHOLD = 80

function Run(self, units, parameter)
    -- unit dead or completely
    if not Spring.ValidUnitID(parameter.unitID) then
        return SUCCESS
    end

    if not self.initialized then
        self.initialized = true
        Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, cropVector(parameter.pos):AsSpringVector(),{})
    end

    local cX, cY, cZ = Spring.GetUnitPosition(parameter.unitID)
	local unitPosition = Vec3(cX, cY, cZ)

    -- succeed condition - unit is close to its target
    if Distance2D(unitPosition, parameter.pos) < THRESHOLD then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end

function Length2D(vec)
	return ((vec.x * vec.x) + (vec.z * vec.z))^0.5
end

function Distance2D(vectorOne, vectorTwo)
	return Length2D(vectorTwo - vectorOne)
end

-- If there is a negative number in the vector, set it to 0.
function cropVector(vec)
    return Vec3(math.max(vec.x,0), math.max(vec.y,0), math.max(vec.z,0))
end