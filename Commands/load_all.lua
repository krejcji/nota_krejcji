function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "Load all units.",
        parameterDefs = {
        }
    }
end

-- Loads all units into the transporter.
-- Preconditions: only friendly and transportable units are on the map.
-- Created for the CTP2 task.
function Run(self, units, parameter)
    if not self.initialized then
        self.initialized = true
        local all_units = Spring.GetAllUnits()
        transporter = units[1]
        for index, value in ipairs(all_units) do
            if value ~= transporter then
                Spring.GiveOrderToUnit(transporter, CMD.LOAD_UNITS,{value},{"shift"})
            end
        end
    end

    if #Spring.GetUnitIsTransporting(transporter) == 6 then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end