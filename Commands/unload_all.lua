function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "Unload all units to given area",
        parameterDefs = {
            {
                name = "unloadLoc",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

-- Unloads all units in a transporter to the unload location.
function Run(self, units, parameter)
    if not self.initialized then
        self.initialized = true
        transporter = units[1]
        Spring.GiveOrderToUnit(transporter, CMD.UNLOAD_UNITS,parameter.unloadLoc,{})
    end

    if #Spring.GetUnitIsTransporting(transporter) == 0 then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end