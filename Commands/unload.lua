function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Unload all units to given area",
        parameterDefs = {
            {
                name = "transporterID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "unloadLoc",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "reservationTable",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end

function Run(self, units, parameter)
    local transporterID = parameter.transporterID
    local unloadLoc = parameter.unloadLoc

    -- transporter dead or completely invalid
    if not Spring.ValidUnitID(transporterID) then
        return SUCCESS
    end

    -- transporter can transport units
    if not UnitDefs[Spring.GetUnitDefID(transporterID)].isTransport then
        return FAILURE
    end

    -- move to target loc
    if not self.initialized then
        self.initialized = true
        local x,y,z = Spring.GetUnitBasePosition(transporterID)
        -- Spring.Echo("Unloading into: " .. x .. " " .. z)
        local params = {x, unloadLoc.center.y, z, 75, 0}
        Spring.GiveOrderToUnit(transporterID, CMD.UNLOAD_UNITS, params, {})
    end

    -- success condition - the transporter is empty
    if #Spring.GetUnitIsTransporting(transporterID) == 0 then
        return SUCCESS
    end

    return RUNNING
end

function Reset(self)
    self.initialized = false
end

function FindFreePlace(safeX, safeZ, radius, resTable)
    local x, z
    while true do
        x = safeX + math.random(-radius, radius)
        z = safeZ + math.random(-radius, radius)
        if (Vec3(x, 0, z)-Vec3(safeX,0,safeZ)):Length() < radius and isEmpty(x,z)
           and notReserved(x,z, resTable) then
            break
        end
    end

    return x, z
end

function isEmpty(x,z)
    local radius = 80
    return #Spring.GetUnitsInCylinder(x, z, radius) == 0
end

function notReserved(x, z, resTable)
    for index, value in ipairs(resTable) do
        if (Vec3(x,0,z) - Vec3(value.x,0,value.z)):Length() < 80 then
            return false
        end
    end
    return true
end